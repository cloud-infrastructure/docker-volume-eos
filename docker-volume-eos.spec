%global gopath %{_tmppath}/gopath
%define debug_package %{nil}
%define _unitdir /lib/systemd/system
%global eos %{gopath}/src/gitlab.cern.ch/cloud-infrastructure/docker-volume-eos

Name:       docker-volume-eos
Version:    0.1.0
Release:    1
Summary:    Docker volume plugin for EOS
Source0:    https://gitlab.cern.ch/cloud-infrastructure/docker-volume-eos/%{name}-%{version}.tar.gz

Group:      Development/Languages
License:    ASL 2.0
URL:        http://gitlab.cern.ch/cloud-infrastructure/docker-volume-eos

BuildRequires: golang
BuildRequires: systemd

%description
Docker volume plugin for the EOS filesystem.
http://eos.readthedocs.io/

%prep
%setup -q -n %{name}-%{version}
rm -rf %{gopath}
mkdir -p %{gopath} %{gopath}/pkg %{gopath}/bin %{gopath}/src %{eos}

%build
cp -R * %{eos}
cd %{eos}
ln -sf `pwd`/vendor/* %{gopath}/src # we need to support go 1.4 (no vendor)
GOPATH=%{gopath} go build .

%install
cd %{eos}

# binary
GOPATH=%{gopath} go install .
install -d %{buildroot}%{_bindir}
install -m 755 %{_tmppath}/gopath/bin/%{name} %{buildroot}%{_bindir}

# default eos config
install -d %{buildroot}%{_sysconfdir}
install -d %{buildroot}%{_sysconfdir}/eos

# kubernetes plugin (symlink)
install -d %{buildroot}%{_libexecdir}/kubernetes/kubelet-plugins/volume/exec/cern~eos
ln -sf %{_bindir}/docker-volume-eos %{buildroot}%{_libexecdir}/kubernetes/kubelet-plugins/volume/exec/cern~eos/eos

%files
%doc LICENSE
%doc README.md
%{_bindir}/%{name}
%{_libexecdir}/kubernetes/kubelet-plugins/volume/exec/cern~eos/eos

%changelog
* Tue Nov 29 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.1.0-1
- First release
