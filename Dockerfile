FROM fedora:23

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN yum install -y \
	wget

RUN wget -q https://ecsft.cern.ch/dist/cvmfs/cvmfs-2.1.20/cvmfs-2.1.20-1.fc21.x86_64.rpm; \
	wget -q https://ecsft.cern.ch/dist/cvmfs/cvmfs-config/cvmfs-config-default-latest.noarch.rpm; \
	yum -y install cvmfs-2.1.20-1.fc21.x86_64.rpm cvmfs-config-default-latest.noarch.rpm

RUN echo $'\n\
CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128" \n\
CVMFS_CACHE_BASE=/var/cache/cvmfs \n\
CVMFS_QUOTA_LIMIT=20000 \n'\
>> /etc/cvmfs/default.local

ADD docker-volume-cvmfs /usr/sbin/docker-volume-cvmfs

RUN chmod 755 /usr/sbin/docker-volume-cvmfs

LABEL Vendor="Red Hat" License=GPLv2
LABEL Version=1.0
LABEL INSTALL="docker run --rm --privileged -v /:/host -e HOST=/host -e CONFDIR=${CONFDIR} -e DATADIR=${DATADIR} -e -e IMAGE=IMAGE -e NAME=NAME IMAGE /bin/install.sh"
LABEL UNINSTALL="docker run --rm --privileged -v /:/host -e HOST=/host -e IMAGE=IMAGE -e NAME=NAME IMAGE /bin/uninstall.sh"
ADD root /

CMD "/usr/sbin/docker-volume-cvmfs"
