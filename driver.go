// Copyright ©2016 CERN
//
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
package main

import (
	"fmt"
	"log"

	"github.com/docker/go-plugins-helpers/volume"
)

type eosDriver struct {
	eos     Eos
	volumes map[string]string
}

func newEosDriver(mountPoint string) eosDriver {
	log.Printf("initializing driver :: mount point: %v\n", mountPoint)
	d := eosDriver{
		eos:     newEos(mountPoint),
		volumes: make(map[string]string),
	}
	return d
}

func (d eosDriver) Create(r volume.Request) volume.Response {
	log.Printf("create %v %v", r.Name, r.Options)

	volumeName := r.Name

	if _, ok := d.volumes[volumeName]; ok {
		return volume.Response{}
	}

	// mount eos
	volumePath, err := d.eos.Mount(volumeName)
	if err != nil {
		return volume.Response{Err: err.Error()}
	}

	// store in map cache for path and other later calls
	d.volumes[volumeName] = volumePath

	return volume.Response{}
}

func (d eosDriver) Path(r volume.Request) volume.Response {
	log.Printf("path %v", r)
	volumeName := r.Name

	if volumePath, ok := d.volumes[volumeName]; ok {
		return volume.Response{Mountpoint: volumePath}
	}

	return volume.Response{}
}

func (d eosDriver) Remove(r volume.Request) volume.Response {
	log.Printf("remove %v", r)

	volumeName := r.Name

	_, err := d.eos.Umount(volumeName)
	if err != nil {
		return volume.Response{Err: err.Error()}
	}

	// remove from map cache
	if _, ok := d.volumes[volumeName]; ok {
		delete(d.volumes, volumeName)
	}

	return volume.Response{}
}

func (d eosDriver) Mount(r volume.Request) volume.Response {
	log.Printf("mount %v", r)
	volumeName := r.Name

	if volumePath, ok := d.volumes[volumeName]; ok {
		return volume.Response{Mountpoint: volumePath}
	}

	return volume.Response{}
}

func (d eosDriver) Unmount(r volume.Request) volume.Response {
	log.Printf("unmount %v", r)
	return volume.Response{}
}

func (d eosDriver) Get(r volume.Request) volume.Response {
	log.Printf("get %v", r)
	volumeName := r.Name

	if volumePath, ok := d.volumes[volumeName]; ok {
		return volume.Response{Volume: &volume.Volume{Name: volumeName, Mountpoint: volumePath}}
	}

	msg := fmt.Sprintf("volume %s does not exist", volumeName)
	log.Printf(msg)
	return volume.Response{Err: msg}
}

func (d eosDriver) List(r volume.Request) volume.Response {
	log.Printf("list %v\n", r)

	volumes := []*volume.Volume{}

	for name, path := range d.volumes {
		volumes = append(volumes, &volume.Volume{Name: name, Mountpoint: path})
	}

	return volume.Response{Volumes: volumes}
}
