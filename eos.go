package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sync"
	"syscall"
)

// Eos is the main EOS object to manage local repositories.
type Eos struct {
	mountPoint string
	m          *sync.Mutex
}

func newEos(mountPoint string) Eos {
	c := Eos{mountPoint: mountPoint, m: &sync.Mutex{}}
	return c
}

// Mount locally mounts the given repo.
func (c Eos) Mount(repo string) (string, error) {

	c.m.Lock()
	defer c.m.Unlock()

	volumePath := filepath.Join(c.mountPoint, repo)

	// check if mount directory exists, create if not
	_, err := os.Lstat(volumePath)
	if err != nil && !os.IsNotExist(err) {
		msg := fmt.Sprintf("failed to check directory :: %v :: %v", volumePath, err)
		return "", Error{repo: repo, msg: msg}
	}
	if os.IsNotExist(err) {
		err = os.Mkdir(volumePath, os.ModePerm)
		if err != nil {
			msg := fmt.Sprintf("failed to create directory :: %v :: %v", volumePath, err)
			return "", Error{repo: repo, msg: msg}
		}
	}

	// check if directory is a fuse mount, mount if not
	statfs := syscall.Statfs_t{}
	err = syscall.Statfs(volumePath, &statfs)
	if err != nil {
		msg := fmt.Sprintf("failed to check mount dir :: %v", err)
		return "", Error{repo: repo, msg: msg}
	}
	if statfs.Type != 0x65735546 {
		log.Printf("mounting %v in %v", repo, volumePath)
		cmd := exec.Command("mount", "-t", "eos", repo, volumePath)
		result, err := cmd.CombinedOutput()
		if err != nil {
			msg := fmt.Sprintf("failed to mount :: %v :: %v", err, string(result))
			return "", Error{repo: repo, msg: msg}
		}
	}

	return volumePath, nil
}

// Umount locally unmounts the given repo.
func (c Eos) Umount(repo string) (string, error) {

	// check dir is a fuse mount, umount if it is
	statfs := syscall.Statfs_t{}
	err := syscall.Statfs(repo, &statfs)
	if err != nil {
		msg := fmt.Sprintf("failed to check unmount dir :: %v :: %v", repo, err)
		return "", Error{repo: repo, msg: msg}
	}
	if statfs.Type == 0x65735546 {
		log.Printf("unmounting %v", repo)
		cmd := exec.Command("umount", repo)
		result, err := cmd.CombinedOutput()
		if err != nil {
			msg := fmt.Sprintf("failed to unmount :: %v :: %v", err, string(result))
			return "", Error{repo: repo, msg: msg}
		}
	}

	return repo, nil
}
