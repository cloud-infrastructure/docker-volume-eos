# docker-volume-eos

This package provides management of EOS repositories in docker and kubernetes.

## Requirements

Docker 1.9.x, Kubernetes 1.2.x.

## Installation

